import React from "react";
import "./Box.css";
import Icon from "../../atoms/Icon/Icon";

const Box = ({
  icon,
  iconStyle,
  title,
  titleStyle,
  description,
  descriptionStyle,
  style
}) => {
  return (
    <div className={style || "box-default-container-style"}>
      {icon && (
        <Icon title={icon} style={iconStyle || "box-default-icon-style"} />
      )}
      {title && (
        <p className={titleStyle || "box-default-title-style"}>{title || ""}</p>
      )}
      {description && (
        <p className={descriptionStyle || "box-default-description-style"}>
          {description || ""}
        </p>
      )}
    </div>
  );
};

export default Box;
