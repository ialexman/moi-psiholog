import React from "react";
import "./Blockquote.css";
import Avatar from "../../atoms/Avatar/Avatar";
import photo from "../../../assets/avatar.jpg";
import classnames from "classnames";
import { Image } from "react-bootstrap";

const Blockquote = ({ text, author, photo, portrait }) => {
  return (
    <div className="blockquote-container">
      {!portrait && (
        <div className="blockquote-text-container">
          <p className="blockquote-text">{text}</p>
        </div>
      )}
      {portrait && (
        <div
          className={classnames(
            "blockquote-text-container",
            "blockquote-text-portrait-container"
          )}
        >
          <div className="blockquote-text-size">
            <p className="blockquote-text">{text}</p>
          </div>

          <div className="blockquote-portrait-container">
            <Avatar
              source={portrait}
              alt={author}
              style="blockquote-portrait"
            />
            <p className="blockquote-portrait-text-container">
              Юлия Семикоп-Осипенко
            </p>
          </div>
        </div>
      )}
      <div>
        {!portrait && (
          <div className="mb-attribution">
            {author && <p className="mb-author">{author}</p>}
            {photo && (
              <Avatar source={photo} alt={author} style="blockquote-photo" />
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default Blockquote;
