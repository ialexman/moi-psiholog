import React from "react";
// import Slider from "react-animated-slider";
// import "react-animated-slider/build/horizontal.css";
import { slides } from "../../atoms/content/slides";
import "./Slider.css";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
const Slider = ({ sliderSize }) => {
  return (
    <Carousel
      showThumbs={false}
      showStatus={false}
      showArrows={false}
      interval={5000}
      transitionTime={1000}
      // infiniteLoop
      useKeyboardArrows
      autoPlay
      stopOnHover
      emulateTouch
    >
      {slides.map((slide, index) => (
        <div className="slider-content-container" key={index}>
          <div style={slider_style.slider_background(slide.image)}>
            <div className="slider-text-container">
              <p className="slide-title">{slide.title}</p>
              <p className="slide-description">{slide.description}</p>
            </div>
          </div>
        </div>
      ))}
    </Carousel>
  );
};

const slider_style = {
  slider_background: image => {
    return {
      backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url('${image}')`,
      backgroundPosition: "center",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      position: "relative",
      height: "100%",
      width: "100%"
    };
  }
};
export default Slider;
