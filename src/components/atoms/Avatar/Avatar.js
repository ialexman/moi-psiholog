import React from "react";

import "./Avatar.css";
const Avatar = ({ source, description, style }) => {
  return (
    <img
      src={source || ""}
      alt={description || ""}
      className={style || "avatarStyle"}
    />
  );
};

export default Avatar;
