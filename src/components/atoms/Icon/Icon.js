import React from "react";
import "./Icon.css";

const Icon = ({ title, style }) => {
  return (
    <div>
      <i
        className={`${title || "fas fa-question"} ${style ||
          "icon-default-style"}`}
      ></i>
    </div>
  );
};

export default Icon;
