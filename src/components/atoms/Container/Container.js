import React from "react";
import style from "./Container.css";
const Container = ({ text, author, photo, children }) => {
  return (
    <div className="container-default-wrap">
      <div className="container-text-container">
        <p className="container-text">{text}</p>
        {children}
      </div>
      <div></div>
    </div>
  );
};

export default Container;
