import avatar from "../../../assets/avatar.jpg";
import library from "../../../assets/library.jfif";
import music from "../../../assets/music.jfif";
import cry from "../../../assets/cry.jfif";
import art from "../../../assets/art.jfif";
import leadership from "../../../assets/leadership.jfif";
import read from "../../../assets/read.jfif";
import be_yourself from "../../../assets/be_yourself.jfif";

export const slides = [
  {
    title: "Можно читать великих классиков",
    description: "и никогда так и не постичь в их произведениях главного...",
    image: library
  },
  {
    title: "Можно слушать Вивальди и Моцарта",
    description: "и жить с пустотой внутри...",
    image: music
  },

  {
    title: "Можно любить живопись,",
    description: "но не видеть прекрасного в самом простом и обычном вокруг...",
    image: art
  },

  {
    title:
      "Можно на память цитировать Гётте и черпать мудрость и глубину в каждом предложении,",
    description: 'а можно умной "уместной" цитатой жестоко «убить»',
    image: cry
  },

  {
    title: "Можно быть эталоном этикета и воспитанности",
    description:
      'но забывать в нужный момент промолчать и "не заметить" чужие "культурные" ошибки.',
    image: leadership
  },
  {
    title: "Можно быть умным, начитанным",
    description:
      "и так и продолжать по привычке всю жизнь «читать морали» окружающим...",
    image: read
  },

  {
    title: "Можно быть кем угодно",
    description: "Вот настоящим - трудно.",
    image: be_yourself
  }
];
