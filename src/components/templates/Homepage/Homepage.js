import React from "react";

import Header from "../../organisms/Header/Header";
import Body from "../../organisms/Body/Body";

import road_video_bg from "../../../assets/road_video_bg.mp4";
///http://xn--c1ajcjcmcbfu9b.com/assets/road_video_bg.mp4
import "./Homepage.css";
const Homepage = () => {
  return (
    <div>
      <Header />
      <Body />
    </div>
  );
};

export default Homepage;
