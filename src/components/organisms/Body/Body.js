import React from "react";
import Box from "../../molecles/Box/Box";
import { boxes } from "../../atoms/content/boxes";
import "./Body.css";
import photo from "../../../assets/avatar_cropped.jpg";
import Avatar from "../../atoms/Avatar/Avatar";
import Blockquote from "../../molecles/Blockquote/Blockquote";
import Container from "../../atoms/Container/Container";

const Body = () => {
  const authorName = "Юлия Семикоп-Осипенко";
  return (
    <div className="body-default-style">
      <section className="boxes-section">
        {boxes.map((boxItem, index) => (
          <div
            className={index !== boxes.length ? "box-item-container" : null}
            key={index}
          >
            <Container>
              <Box
                icon={boxItem.icon}
                iconStyle="box-icon"
                description={boxItem.description}
              />
            </Container>
          </div>
        ))}
      </section>

      <section className="cite-section">
        <div className="cite-container">
          <Blockquote
            author={authorName}
            photo={photo}
            text="Вся моя работа основана на убеждении в том, что человек, обращаясь
            ко мне за помощью, способен сам определить причины и найти способ
            решения своих проблем - нужна лишь моя поддержка, поддержка
            специалиста. Считаю очень важным в выбранном мною методе работы, что
            именно клиент осуществляет направляющие изменения. Моя работа
            проходит в форме диалога между мной и клиентом. И самое важное в
            этом диалоге - эмоциональная атмосфера доверия, уважения и
            неоценивающего понимания. Это позволяет клиенту чувствовать, что его
            воспринимают таким, какой он есть, что он может говорить обо всем,
            не опасаясь осуждения или неодобрения и именно это является главным
            ключом к решению огромного спектра психо-эмоциональных проблем
            клиента и обеспечивает достижение необходимого психотерапевтического
            эффекта."
          />
        </div>
      </section>
    </div>
  );
};

export default Body;
