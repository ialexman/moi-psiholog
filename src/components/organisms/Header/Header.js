import React from "react";
import logo from "../../../assets/logo_colored.png";

import "./Header.css";
import Navbar from "../../molecles/Navbar/Navbar";
import Slider from "../../molecles/Slider/Slider";
const Header = ({ logoSource, logoDescription }) => {
  return (
    <div>
      <img
        src={logoSource || logo}
        alt={logoDescription || "Мой психолог Юлия Семикоп Осипенко"}
        className="logo"
      />
      {/* <Navbar /> */}

      {/* <div className="carousel-block"> */}
      <Slider />
      {/* </div>  */}
    </div>
  );
};

export default Header;
