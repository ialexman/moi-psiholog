import React from "react";
import "./App.css";

import Homepage from "../src/components/templates/Homepage/Homepage";
const App = () => {
  return (
    <div className="global-style">
      <Homepage />
    </div>
  );
};

export default App;
